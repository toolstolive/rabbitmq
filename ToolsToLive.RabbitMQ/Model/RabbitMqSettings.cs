﻿using System;
using RabbitMQ.Client;

namespace ToolsToLive.RabbitMQ.Model
{
    public class RabbitMqSettings
    {
        /// <summary>
        /// RabbitMQ connection string. If defined - fields Host, Port, UserName and Password are ignored
        /// </summary>
        public string ConnectionString { get; set; }

        public string Host { get; set; }

        public int? Port { get; set; }

        public string UserName { get; set; }

        public string Password { get; set; }

        /// <summary>
        /// An arbitrary connection name. Displayed in the RabbitMQ manager UI.
        /// </summary>
        public string ConnectionName { get; set; } = ConnectionNameDefault;

        public static string ConnectionNameDefault { get; } = "RabbitMQ_DefaultConnectionName";

        // Application name (used to name queues)
        public string AppName { get; set; } = AppNameDefault;

        public static string AppNameDefault { get; } = "RabbitMQ_DefaultAppName";

        // Application version (used to name queues)
        public string AppVersion { get; set; } = AppVersionDefault;

        public static string AppVersionDefault { get; } = "v1";

        /// <summary>
        /// Delay 1 (10 sec by default)
        /// </summary>
        public TimeSpan Delay1 { get; set; } = Delay1Default;

        public static TimeSpan Delay1Default { get; } = TimeSpan.FromSeconds(10);

        /// <summary>
        /// Delay 2 (30 sec by default)
        /// </summary>
        public TimeSpan Delay2 { get; set; } = Delay2Default;

        public static TimeSpan Delay2Default { get; } = TimeSpan.FromSeconds(30);

        /// <summary>
        /// Delay 3 (1 min by default)
        /// </summary>
        public TimeSpan Delay3 { get; set; } = Delay3Default;

        public static TimeSpan Delay3Default { get; } = TimeSpan.FromMinutes(1);

        /// <summary>
        /// Delay 4 (5 min by default)
        /// </summary>
        public TimeSpan Delay4 { get; set; } = Delay4Default;

        public static TimeSpan Delay4Default { get; } = TimeSpan.FromMinutes(5);

        /// <summary>
        /// Delay 5 (30 min by default)
        /// </summary>
        public TimeSpan Delay5 { get; set; } = Delay5Default;

        public static TimeSpan Delay5Default { get; } = TimeSpan.FromMinutes(30);

        /// <summary>
        /// Delay 6 (1 hour by default)
        /// </summary>
        public TimeSpan Delay6 { get; set; } = Delay6Default;

        public static TimeSpan Delay6Default { get; } = TimeSpan.FromHours(1);

        /// <summary>
        /// Delay 7 (24 hours by default)
        /// </summary>
        public TimeSpan Delay7 { get; set; } = Delay7Default;

        public static TimeSpan Delay7Default { get; } = TimeSpan.FromHours(24);

        /// <summary>
        /// Set to a value greater than one to enable concurrent processing. For a concurrency greater than one <see cref="IAsyncBasicConsumer"/>
        /// will be offloaded to the worker thread pool so it is important to choose the value for the concurrency wisely to avoid thread pool overloading.
        /// <see cref="IAsyncBasicConsumer"/> can handle concurrency much more efficiently due to the non-blocking nature of the consumer.
        /// Defaults to 1.
        /// </summary>
        /// <remarks>
        /// For concurrency greater than one this removes the guarantee that consumers handle messages in the order they receive them.
        /// In addition to that consumers need to be thread/concurrency safe.
        /// </remarks>
        public ushort ConsumerDispatchConcurrency { get; set; } = Constants.DefaultConsumerDispatchConcurrency;
    }
}
