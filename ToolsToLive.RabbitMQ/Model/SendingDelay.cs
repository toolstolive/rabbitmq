﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace ToolsToLive.RabbitMQ.Model
{
    [JsonConverter(typeof(StringEnumConverter))]
    public enum SendingDelay
    {
        /// <summary>
        /// Without delay (by default)
        /// </summary>
        NoDelay,

        /// <summary>
        /// 10 sec by default
        /// </summary>
        Delay1,

        /// <summary>
        /// 30 sec by default
        /// </summary>
        Delay2,

        /// <summary>
        /// 1 min by default
        /// </summary>
        Delay3,

        /// <summary>
        /// 5 min by default
        /// </summary>
        Delay4,

        /// <summary>
        /// 30 min by default
        /// </summary>
        Delay5,

        /// <summary>
        /// 1 hour by default
        /// </summary>
        Delay6,

        /// <summary>
        /// 24 hours by default
        /// </summary>
        Delay7,
    }
}
