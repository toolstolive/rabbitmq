﻿using System;
using System.Threading.Tasks;
using ToolsToLive.RabbitMQ.Interfaces;

namespace ToolsToLive.RabbitMQ.Model
{
    public class Subscription : IAsyncDisposable
    {
        private readonly IMessageListener _listener;
        private readonly string _queueName;
        private readonly string _eventType;
        private readonly string _appKey;

        public Subscription(
            IMessageListener listener,
            string queueName,
            string eventType,
            string appKey)
        {
            _listener = listener;
            _queueName = queueName;
            _eventType = eventType;
            _appKey = appKey;
        }

        public string QueueName => _queueName;

        public string EventType => _eventType;

        public string AppKey => _appKey;

        /// <summary>
        /// Stopping a Queue listener.
        /// Does not delete the queue. With a new subscription, the subscriber will receive all the accumulated messages.
        /// To delete a queue, use IEventBus.Unsubscribe()
        /// </summary>
        public async ValueTask UnsubscribeAsync()
        {
            await DisposeAsync();
        }

        /// <summary>
        /// Stopping a Queue listener.
        /// Does not delete the queue. With a new subscription, the subscriber will receive all the accumulated messages.
        /// To delete a queue, use IEventBus.Unsubscribe()
        /// </summary>
        public async ValueTask DisposeAsync()
        {
            await _listener.DisposeAsync();
        }
    }
}
