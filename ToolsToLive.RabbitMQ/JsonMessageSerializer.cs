﻿using System;
using System.Text;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using ToolsToLive.RabbitMQ.Interfaces;

namespace ToolsToLive.RabbitMQ
{
    public class JsonMessageSerializer : IMessageSerializer
    {
        private readonly ILogger<JsonMessageSerializer> _logger;

        public JsonMessageSerializer(ILogger<JsonMessageSerializer> logger)
        {
            _logger = logger;
        }

        public TMessage Deserialize<TMessage>(ReadOnlyMemory<byte> body) where TMessage : new()
        {
            var messageContent = Encoding.UTF8.GetString(body.ToArray());
            _logger.LogTrace($"Message to deserialize: {messageContent}");
            return JsonConvert.DeserializeObject<TMessage>(messageContent);
        }

        public byte[] Serialize<TMessage>(TMessage message) where TMessage : new()
        {
            var msg = JsonConvert.SerializeObject(message);
            _logger.LogTrace($"Serialized message: {msg}");
            return Encoding.UTF8.GetBytes(msg);
        }
    }
}
