﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using RabbitMQ.Client;
using System;
using System.Threading;
using System.Threading.Tasks;
using ToolsToLive.RabbitMQ.Client;
using ToolsToLive.RabbitMQ.ClientsPool;
using ToolsToLive.RabbitMQ.Interfaces;
using ToolsToLive.RabbitMQ.Interfaces.Client;
using ToolsToLive.RabbitMQ.Interfaces.ClientsPool;
using ToolsToLive.RabbitMQ.Interfaces.RoutingBuilders;
using ToolsToLive.RabbitMQ.Model;
using ToolsToLive.RabbitMQ.RoutingBuilders;

namespace ToolsToLive.RabbitMQ
{
    public static class DependencyInjectionExtensions
    {
        public static IServiceCollection AddRabbitMqMessageHandler(this IServiceCollection services, IConfigurationSection configurationSection)
        {
            if (string.IsNullOrWhiteSpace(configurationSection[nameof(RabbitMqSettings.ConnectionString)]) &&
                    (
                    string.IsNullOrWhiteSpace(configurationSection[nameof(RabbitMqSettings.Host)]) ||
                    configurationSection[nameof(RabbitMqSettings.Port)] == null ||
                    string.IsNullOrWhiteSpace(configurationSection[nameof(RabbitMqSettings.UserName)]) ||
                    string.IsNullOrWhiteSpace(configurationSection[nameof(RabbitMqSettings.Password)])
                    )
                )
            {
                throw new ArgumentException("ConnectionString (or Host, Port, UserName and Password) must be set in configuration", nameof(configurationSection));
            }

            services.AddOptions();
            services.Configure<RabbitMqSettings>(configurationSection);

            services.AddSingleton<IConnectionFactory>(sp =>
            {
                var settings = sp.GetRequiredService<IOptions<RabbitMqSettings>>();
                if (!string.IsNullOrWhiteSpace(settings.Value.ConnectionString))
                {
                    return new ConnectionFactory
                    {
                        Uri = new Uri(settings.Value.ConnectionString),
                        ConsumerDispatchConcurrency = settings.Value.ConsumerDispatchConcurrency,
                    };
                }
                else
                {
                    if (string.IsNullOrWhiteSpace(settings.Value.Host) ||
                        !settings.Value.Port.HasValue ||
                        string.IsNullOrWhiteSpace(settings.Value.UserName) ||
                        string.IsNullOrWhiteSpace(settings.Value.Password))
                    {
                        throw new ArgumentException("ConnectionString (or Host, Port, UserName and Password) must be set in configuration", nameof(configurationSection));
                    }

                    var conFactory = new ConnectionFactory
                    {
                        HostName = settings.Value.Host,
                        Port = settings.Value.Port.Value,
                        UserName = settings.Value.UserName,
                        Password = settings.Value.Password,
                        ConsumerDispatchConcurrency = settings.Value.ConsumerDispatchConcurrency,
                        //Ssl = new SslOption
                        //{
                        //    Enabled = true,
                        //    CertificateValidationCallback = (sender, certificate, chain, errors) => true,
                        //}
                    };
                    //conFactory.Ssl = new SslOption
                    //{
                    //    Enabled = true,
                    //    CertificateValidationCallback = (sender, certificate, chain, errors) => true,
                    //};
                    return conFactory;
                }
            });

            services.AddSingleton<IRabbitMqConnectionProvider, RabbitMqConnectionProvider>();
            services.AddSingleton<IRabbitMqClientFactory, RabbitMqClientFactory>(); // Each client uses its own chanel (but one connection for all of them)
            services.AddSingleton<IRabbitMqClientsPool, RabbitMqClientsPool>();
            services.AddSingleton<IMessageSerializer, JsonMessageSerializer>();
            services.AddSingleton<IExchangeNameBuilder, ExchangeNameBuilder>();
            services.AddSingleton<IQueueNameBuilder, QueueNameBuilder>();
            services.AddSingleton<IRoutingKeyBuilder, RoutingKeyBuilder>();
            services.AddSingleton<IRabbitMqConfigurator, RabbitMqConfigurator>();
            services.AddSingleton<IMessageSender, MessageSender>();
            services.AddSingleton<IMessageListenerFactory, MessageListenerFactory>();
            services.AddSingleton<IEventBus, EventBus>();

            return services;
        }

        public static async Task ConfigureRabbitMQAsync<TRoute>(this IServiceProvider serviceProvider, CancellationToken cancellationToken = default)
            where TRoute : Enum
        {
            var configurator = serviceProvider.GetRequiredService<IRabbitMqConfigurator>();
            await configurator.ConfigureAsync<TRoute>(cancellationToken);
        }
    }
}
