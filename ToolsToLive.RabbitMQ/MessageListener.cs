﻿using System.Threading.Tasks;
using System.Threading;
using System;
using Microsoft.Extensions.Logging;
using ToolsToLive.RabbitMQ.Interfaces.ClientsPool;
using ToolsToLive.RabbitMQ.Interfaces;

namespace ToolsToLive.RabbitMQ
{
    public class MessageListener : IMessageListener
    {
        private readonly IMessageSerializer _messageSerializer;
        private readonly ILogger<MessageListener> _logger;

        private readonly IRabbitMqPoolClient _clientPool;

        /// <summary>
        /// Initializes a new instance of the <see cref="MessageListener"/> class.
        /// </summary>
        /// <param name="clientsPool"></param>
        /// <param name="messageSerializer"></param>
        /// <param name="logger"></param>
        internal MessageListener(
            IRabbitMqClientsPool clientsPool,
            IMessageSerializer messageSerializer,
            ILogger<MessageListener> logger)
        {
            _clientPool = clientsPool.GetPoolClient();
            _messageSerializer = messageSerializer;
            _logger = logger;
        }

        /// <inheritdoc/>
        public async Task ListenAsync<TMessage>(string queueName, Func<TMessage, Task> onReceived, ushort prefetchCount, CancellationToken cancellationToken)
            where TMessage : new()
        {
            _logger.LogDebug("Starting to listen to the queue {queueName}", queueName);

            var rabbitMqClient = await _clientPool.GetRabbitMqClientAsync(cancellationToken);
            await rabbitMqClient.ListenAsync(queueName, async (messageBody) =>
                {
                    TMessage msg;
                    try
                    {
                        msg = _messageSerializer.Deserialize<TMessage>(messageBody);
                    }
                    catch (Exception ex)
                    {
                        _logger.LogError(ex, $"Deserialization error of the message received from RabbitMQ.");
                        return;
                    }

                    try
                    {
                        await onReceived(msg);
                    }
                    catch (Exception ex)
                    {
                        _logger.LogError(ex, $"Error when processing a message received from RabbitMQ.");
                    }
                }, prefetchCount, cancellationToken);
        }

        /// <summary>
        /// Stops listening to the queue and disposes client
        /// </summary>
        public async ValueTask DisposeAsync()
        {
            var rabbitMqClient = await _clientPool.GetRabbitMqClientAsync(CancellationToken.None);
            await rabbitMqClient.StopListeningAsync();

            // returning client to the pool
            _clientPool.Dispose();
        }
    }
}
