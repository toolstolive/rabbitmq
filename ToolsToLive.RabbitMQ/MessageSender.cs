﻿using System.Threading.Tasks;
using System.Threading;
using System;
using Microsoft.Extensions.Options;
using ToolsToLive.RabbitMQ.Interfaces;
using ToolsToLive.RabbitMQ.Interfaces.ClientsPool;
using ToolsToLive.RabbitMQ.Interfaces.RoutingBuilders;
using ToolsToLive.RabbitMQ.Model;

namespace ToolsToLive.RabbitMQ
{
    public class MessageSender : IMessageSender
    {
        private readonly IRabbitMqClientsPool _clientsPool;
        private readonly IMessageSerializer _messageSerializer;
        private readonly IExchangeNameBuilder _exchangeNameBuilder;
        private readonly IRoutingKeyBuilder _routingKeyBuilder;
        private readonly IOptions<RabbitMqSettings> _options;

        public MessageSender(
            IRabbitMqClientsPool clientsPool,
            IMessageSerializer messageSerializer,
            IExchangeNameBuilder exchangeNameBuilder,
            IRoutingKeyBuilder routingKeyBuilder,
            IOptions<RabbitMqSettings> options)
        {
            _clientsPool = clientsPool;
            _messageSerializer = messageSerializer;
            _exchangeNameBuilder = exchangeNameBuilder;
            _routingKeyBuilder = routingKeyBuilder;
            _options = options;
        }

        public async Task SendAsync<TMessage>(TMessage message, string exchangeName, string route, TimeSpan? delay = null, CancellationToken cancellationToken = default)
            where TMessage : new()
        {
            using (var poolClient = _clientsPool.GetPoolClient())
            {
                var rabbitMqClient = await poolClient.GetRabbitMqClientAsync(cancellationToken);
                await rabbitMqClient.SendAsync(
                    _messageSerializer.Serialize(message),
                    exchangeName,
                    route,
                    delay,
                    cancellationToken);
            }
        }

        public async Task SendAsync<TMessage>(TMessage message, string exchangeName, string route, SendingDelay delay, CancellationToken cancellationToken = default)
            where TMessage : new()
        {
            await SendAsync(message,
                exchangeName,
                route,
                GetDelayTime(delay),
                cancellationToken);
        }

        public async Task SendAsync<TRoute, TMessage>(TMessage message, TRoute targetRoute, CancellationToken cancellationToken = default)
            where TMessage : new() where TRoute : Enum
        {
            await SendAsync(message,
                _exchangeNameBuilder.Build(),
                _routingKeyBuilder.Build(targetRoute),
                null,
                cancellationToken);
        }

        public async Task SendAsync<TRoute, TMessage>(TMessage message, TRoute targetRoute, SendingDelay delay, CancellationToken cancellationToken = default)
            where TMessage : new() where TRoute : Enum
        {
            await SendAsync(message,
                _exchangeNameBuilder.BuildForDelayedQueues(),
                _routingKeyBuilder.Build(targetRoute, delay),
                delay,
                cancellationToken);
        }

        private TimeSpan GetDelayTime(SendingDelay delay)
        {
            TimeSpan delayTimeSpan;

            switch (delay)
            {
                case SendingDelay.Delay1:
                    delayTimeSpan = _options.Value.Delay1;
                    break;
                case SendingDelay.Delay2:
                    delayTimeSpan = _options.Value.Delay2;
                    break;
                case SendingDelay.Delay3:
                    delayTimeSpan = _options.Value.Delay3;
                    break;
                case SendingDelay.Delay4:
                    delayTimeSpan = _options.Value.Delay4;
                    break;
                case SendingDelay.Delay5:
                    delayTimeSpan = _options.Value.Delay5;
                    break;
                case SendingDelay.Delay6:
                    delayTimeSpan = _options.Value.Delay6;
                    break;
                case SendingDelay.Delay7:
                    delayTimeSpan = _options.Value.Delay7;
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(delay), delay, "Unexpected value.");
            }

            return delayTimeSpan;
        }
    }
}
