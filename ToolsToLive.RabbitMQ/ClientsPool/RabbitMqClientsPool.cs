using System;
using System.Collections.Concurrent;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using ToolsToLive.RabbitMQ.Interfaces.Client;
using ToolsToLive.RabbitMQ.Interfaces.ClientsPool;

namespace ToolsToLive.RabbitMQ.ClientsPool
{
    public class RabbitMqClientsPool : IRabbitMqClientsPool
    {
        private readonly IRabbitMqClientFactory _clientFactory;
        private readonly ILogger<RabbitMqClientsPool> _logger;

        private readonly ConcurrentBag<IRabbitMqClient> _allClients;
        private readonly ConcurrentBag<IRabbitMqClient> _availableClients;

        private bool _stopped;
        private readonly SemaphoreSlim _startStopLock = new SemaphoreSlim(1, 1);

        public RabbitMqClientsPool(
            IRabbitMqClientFactory clientFactory,
            ILogger<RabbitMqClientsPool> logger)
        {
            _clientFactory = clientFactory;
            _logger = logger;

            _allClients = new ConcurrentBag<IRabbitMqClient>();
            _availableClients = new ConcurrentBag<IRabbitMqClient>();
        }

        public IRabbitMqPoolClient GetPoolClient()
        {
            return new RabbitMqPoolClient(this);
        }

        internal async Task<IRabbitMqClient> GetClientAsync(CancellationToken cancellationToken = default)
        {
            if (_availableClients.TryTake(out var client))
            {
                return client;
            }

            return await CreateClientAsync(cancellationToken);
        }

        internal void ReleaseClient(IRabbitMqClient client)
        {
            _availableClients.Add(client);
        }

        private async Task<IRabbitMqClient> CreateClientAsync(CancellationToken cancellationToken = default)
        {
            try
            {
                await _startStopLock.WaitAsync(cancellationToken);
                if (_stopped)
                {
                    throw new Exception("The RabbitMQ client pool has been stopped");
                }

                var client = _clientFactory.CreateClient();
                _allClients.Add(client);

                if (_allClients.Count > 150)
                {
                    _logger.LogWarning("Suspiciously many clients created, make sure all clients are released into the pool after use");
                }

                return client;
            }
            finally
            {
                _startStopLock.Release();
            }
        }

        public async ValueTask DisposeAsync()
        {
            try
            {
                await _startStopLock.WaitAsync();

                if (_stopped)
                {
                    return;
                }
                _stopped = true;

                while (_availableClients.TryTake(out _))
                {
                    // just clear collection
                }

                while (_allClients.TryTake(out var client))
                {
                    await client.DisposeAsync();
                }
            }
            finally
            {
                _startStopLock.Release();
            }
        }
    }
}
