﻿using System.Threading.Tasks;
using System.Threading;
using ToolsToLive.RabbitMQ.Interfaces.Client;
using ToolsToLive.RabbitMQ.Interfaces.ClientsPool;

namespace ToolsToLive.RabbitMQ.ClientsPool
{
    public class RabbitMqPoolClient : IRabbitMqPoolClient
    {
        private readonly RabbitMqClientsPool _pool;
        private IRabbitMqClient _client;

        internal RabbitMqPoolClient(
            RabbitMqClientsPool pool)
        {
            _pool = pool;
        }

        public void Dispose()
        {
            if (_client != null)
            {
                _pool.ReleaseClient(_client);
            }

            _client = null;
        }

        public async Task<IRabbitMqClient> GetRabbitMqClientAsync(CancellationToken cancellationToken)
        {
            if (_client != null)
            {
                return _client;
            }

            _client = await _pool.GetClientAsync(cancellationToken);
            return _client;
        }
    }
}
