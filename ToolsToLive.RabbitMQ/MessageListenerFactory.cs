﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using ToolsToLive.RabbitMQ.Interfaces;
using ToolsToLive.RabbitMQ.Interfaces.ClientsPool;
using ToolsToLive.RabbitMQ.Interfaces.RoutingBuilders;

namespace ToolsToLive.RabbitMQ
{
    public class MessageListenerFactory : IMessageListenerFactory
    {
        private readonly IServiceProvider _serviceProvider;
        private readonly IRabbitMqClientsPool _clientsPool;
        private readonly IQueueNameBuilder _queueNameBuilder;
        private readonly IMessageSerializer _messageSerializer;

        public MessageListenerFactory(
            IServiceProvider serviceProvider,
            IRabbitMqClientsPool clientsPool,
            IQueueNameBuilder queueNameBuilder,
            IMessageSerializer messageSerializer)
        {
            _serviceProvider = serviceProvider;
            _clientsPool = clientsPool;
            _queueNameBuilder = queueNameBuilder;
            _messageSerializer = messageSerializer;
        }

        public async Task<IMessageListener> CreateAndStartListeningAsync<TMessage>(string queueName, Func<TMessage, Task> onReceived, ushort prefetchCount, CancellationToken cancellationToken = default)
            where TMessage : new()
        {
            var logger = _serviceProvider.GetRequiredService<ILogger<MessageListener>>();
            var listener = new MessageListener(_clientsPool, _messageSerializer, logger);
            await listener.ListenAsync(queueName, onReceived, prefetchCount, cancellationToken);
            return listener;
        }

        public async Task<IMessageListener> CreateAndStartListeningAsync<TRoute, TMessage>(TRoute targetRoute, Func<TMessage, Task> onReceived, ushort prefetchCount, CancellationToken cancellationToken = default)
            where TMessage : new() where TRoute : Enum
        {
            return await CreateAndStartListeningAsync(
                _queueNameBuilder.Build(targetRoute),
                onReceived,
                prefetchCount);
        }
    }
}
