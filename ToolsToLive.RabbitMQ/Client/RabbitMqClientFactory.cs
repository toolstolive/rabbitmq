﻿using System;
using System.Threading.Tasks;
using System.Threading;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using RabbitMQ.Client;
using ToolsToLive.RabbitMQ.Interfaces.Client;

namespace ToolsToLive.RabbitMQ.Client
{
    public class RabbitMqClientFactory : IRabbitMqClientFactory
    {
        private readonly IServiceProvider _serviceProvider;
        private readonly IRabbitMqConnectionProvider _connectionProvider;
        private readonly ILogger<RabbitMqClientFactory> _logger;

        public RabbitMqClientFactory(
            IServiceProvider serviceProvider,
            IRabbitMqConnectionProvider connectionProvider,
            ILogger<RabbitMqClientFactory> logger)
        {
            _serviceProvider = serviceProvider;
            _connectionProvider = connectionProvider;
            _logger = logger;
        }

        public IRabbitMqClient CreateClient()
        {
            var logger = _serviceProvider.GetRequiredService<ILogger<RabbitMqClient>>();
            var client = new RabbitMqClient(_connectionProvider, logger);

            _logger.LogDebug("RabbitMQ client created");
            return client;
        }
    }

    public class RabbitMqChannelFactory
    {
        private IChannel _channel;
        private readonly SemaphoreSlim _reconnectLock = new SemaphoreSlim(1, 1);
        private readonly IRabbitMqConnectionProvider _rabbitMqConnectionProvider;
        private readonly ILogger _logger;

        public RabbitMqChannelFactory(
            IRabbitMqConnectionProvider rabbitMqConnectionProvider,
            ILogger logger)
        {
            _rabbitMqConnectionProvider = rabbitMqConnectionProvider;
            _logger = logger;
        }

        public async Task<IChannel> GetChannelAsync()
        {
            if (_channel != null && !_channel.IsClosed)
            {
                return _channel;
            }

            try
            {
                await _reconnectLock.WaitAsync();

                if (_channel != null && !_channel.IsClosed)
                {
                    return _channel;
                }

                var connection = await _rabbitMqConnectionProvider.GetConnectionAsync();
                _channel = await connection.CreateChannelAsync();

                _logger.LogDebug("Connection restored");
                return _channel;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Connection restore error");
                throw;
            }
            finally
            {
                _reconnectLock.Release();
            }
        }
    }
}
