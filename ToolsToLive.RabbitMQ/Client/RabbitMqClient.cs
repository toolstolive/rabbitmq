﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using OpenTelemetry;
using OpenTelemetry.Context.Propagation;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using ToolsToLive.RabbitMQ.Interfaces.Client;
using ToolsToLive.RabbitMQ.Observability;

namespace ToolsToLive.RabbitMQ.Client
{
    public class RabbitMqClient : IRabbitMqClient
    {
        private readonly ILogger<RabbitMqClient> _logger;
        private readonly RabbitMqChannelFactory _rabbitMqChannelFactory;

        private AsyncEventingBasicConsumer _consumer;
        private string _consumerTag;
        private bool _isListening;
        private bool _disposed;

        internal RabbitMqClient(
            IRabbitMqConnectionProvider rabbitMqConnectionProvider,
            ILogger<RabbitMqClient> logger)
        {
            _logger = logger;
            _rabbitMqChannelFactory = new RabbitMqChannelFactory(rabbitMqConnectionProvider, logger);
        }

        public async Task SendAsync(ReadOnlyMemory<byte> message, string exchangeName, string routingKey, TimeSpan? delay = null, CancellationToken cancellationToken = default)
        {
            var activity = Tracing.ActivitySource.StartActivity("RabbitMQ.Send", ActivityKind.Producer);
            activity?.AddTag("exchange.name", exchangeName);
            activity?.AddTag("routing.key", routingKey);
            if (delay.HasValue)
                activity?.AddTag("delay", delay.Value.ToString());

            try
            {
                var channel = await _rabbitMqChannelFactory.GetChannelAsync();
                var properties = new BasicProperties
                {
                    DeliveryMode = DeliveryModes.Persistent,
                    ContentType = "application/json",
                };

                // if anyone is listening to ToolsToLive.RabbitMQ.Observability.Tracing.ActivitySourceName, set context of Activity to message headers
                if (activity != null)
                {
                    if (properties.Headers == null)
                    {
                        properties.Headers = new Dictionary<string, object>();
                    }

                    var propagationContext = new PropagationContext(Activity.Current.Context, Baggage.Current);
                    Propagators.DefaultTextMapPropagator.Inject(
                        propagationContext,
                        properties.Headers,
                        (headers, key, value) =>
                        {
                            var val = Encoding.UTF8.GetBytes(value);
                            headers.Add(key, val);
                        });
                }

                if (delay.HasValue)
                {
                    if (delay.Value < TimeSpan.Zero)
                        delay = TimeSpan.Zero;

                    properties.Expiration = Math.Round(delay.Value.TotalMilliseconds).ToString(CultureInfo.InvariantCulture);
                }

                await channel.BasicPublishAsync(exchangeName, routingKey, true, properties, message, cancellationToken);
                _logger.LogDebug("Message sent to {exchangeName}", exchangeName);
            }
            catch (Exception ex)
            {
                activity?.SetStatus(ActivityStatusCode.Error, $"Unable to send message to {exchangeName}, message: {ex.Message}");
                throw;
            }
            finally
            {
                activity?.Dispose();
            }
        }

        public async Task ListenAsync(string queueName, Func<ReadOnlyMemory<byte>, Task> onReceived, ushort prefetchCount, CancellationToken cancellationToken = default)
        {
            var channel = await _rabbitMqChannelFactory.GetChannelAsync();
            _consumer = new AsyncEventingBasicConsumer(channel);

            _consumer.ReceivedAsync += async (_, eventArgs) =>
            {
                // try to get the parent context (from another service) to keep tracing between services
                var propagationContext = Propagators.DefaultTextMapPropagator.Extract(
                    default,
                    eventArgs.BasicProperties.Headers,
                    (headers, key) =>
                    {
                        if (headers == null)
                        {
                            return new string[] { };
                        }

                        return headers.Where(x => x.Key == key)
                            .Select(x =>
                            {
                                try
                                {
                                    var str = Encoding.UTF8.GetString(x.Value as byte[]);
                                    return str;
                                }
                                catch (Exception)
                                {
                                    return null;
                                }
                            })
                            .Where(x => x != null);
                    });

                var activity = Tracing.ActivitySource.StartActivity(
                    "RabbitMQ.Received",
                    kind: ActivityKind.Consumer,
                    parentContext: propagationContext.ActivityContext);

                Baggage.Current = propagationContext.Baggage;

                try
                {
                    _logger.LogDebug("Received a message from the queue {queueName}", queueName);

                    if (!_isListening)
                    {
                        _logger.LogDebug("The listener has been stopped");
                        return;
                    }

                    await onReceived(eventArgs.Body)
                        .ContinueWith(async (task) =>
                        {
                            try
                            {
                                if (task.IsCompleted && !task.IsCanceled && !task.IsFaulted && task.Status == TaskStatus.RanToCompletion)
                                {
                                    await SingleAckAsync(eventArgs.DeliveryTag, CancellationToken.None);
                                }
                            }
                            catch (ThreadAbortException)
                            {
                            }
                            catch (Exception ex)
                            {
                                _logger.LogError(ex, ex.Message);
                                Activity.Current?.SetStatus(ActivityStatusCode.Error, "Unable send acknowledge");
                            }
                        },
                        cancellationToken);
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, ex.Message);
                }
                finally
                {
                    activity?.Dispose();
                }
            };

            _isListening = true;
            await channel.BasicQosAsync(0, prefetchCount, false, cancellationToken);
            _consumerTag = await channel.BasicConsumeAsync(queueName, false, _consumer, cancellationToken);
        }

        public async Task SingleAckAsync(ulong deliveryTag, CancellationToken cancellationToken)
        {
            if (deliveryTag <= 0)
            {
                return;
            }
            var channel = await _rabbitMqChannelFactory.GetChannelAsync();
            await channel.BasicAckAsync(deliveryTag, false, cancellationToken);
            _logger.LogDebug("Acknowledge sent");
        }

        public async Task StopListeningAsync(CancellationToken cancellationToken = default)
        {
            _logger.LogDebug("Stop listening to messages");
            var channel = await _rabbitMqChannelFactory.GetChannelAsync();

            _isListening = false;
            if (channel != null && channel.IsOpen && _consumer != null && _consumer.IsRunning)
            {
                try
                {
                    await channel.BasicCancelAsync(consumerTag: _consumerTag, cancellationToken: cancellationToken);
                    _logger.LogDebug("Consumer stopped");
                }
                catch (Exception exception)
                {
                    _logger.LogError(exception, exception.Message);
                }
            }
            else
            {
                _logger.LogDebug("There is nothing to stop");
            }
        }

        public async Task QueueDeclareAsync(string queueName, Dictionary<string, object> arguments = null, CancellationToken cancellationToken = default)
        {
            var channel = await _rabbitMqChannelFactory.GetChannelAsync();
            await channel.QueueDeclareAsync(
                queue: queueName,
                durable: true,
                exclusive: false,
                autoDelete: false,
                arguments: arguments,
                cancellationToken: cancellationToken);
        }

        public async Task ExchangeDeclareAsync(string exchangeName, CancellationToken cancellationToken = default)
        {
            var channel = await _rabbitMqChannelFactory.GetChannelAsync();
            await channel.ExchangeDeclareAsync(
                exchange: exchangeName,
                type: "topic",
                durable: true,
                autoDelete: false,
                arguments: null,
                cancellationToken: cancellationToken);
        }

        public async Task QueueBindAsync(string exchangeName, string queueName, string routingKey, CancellationToken cancellationToken = default)
        {
            var channel = await _rabbitMqChannelFactory.GetChannelAsync();
            await channel.QueueBindAsync(
                queue: queueName,
                exchange: exchangeName,
                routingKey: routingKey,
                arguments: null,
                cancellationToken: cancellationToken);
        }

        public async Task DeleteQueueAsync(string queueName, CancellationToken cancellationToken = default)
        {
            var channel = await _rabbitMqChannelFactory.GetChannelAsync();
            await channel.QueueDeleteAsync(
                queue: queueName,
                cancellationToken: cancellationToken);
        }

        public async Task DeleteExchangeAsync(string exchangeName, CancellationToken cancellationToken = default)
        {
            var channel = await _rabbitMqChannelFactory.GetChannelAsync();
            await channel.ExchangeDeleteAsync(
                exchange: exchangeName,
                cancellationToken: cancellationToken);
        }

        public async ValueTask DisposeAsync()
        {
            await DisposeAsync(true, CancellationToken.None);
        }

        protected virtual async Task DisposeAsync(bool disposing, CancellationToken cancellationToken = default)
        {
            if (_disposed)
            {
                return;
            }

            if (disposing)
            {
                _disposed = true;

                if (_isListening)
                {
                    await StopListeningAsync(cancellationToken);
                }

                var channel = await _rabbitMqChannelFactory.GetChannelAsync();

                if (!channel.IsClosed)
                {
                    await channel.CloseAsync(cancellationToken);
                }

                channel.Dispose();
            }
        }
    }
}
