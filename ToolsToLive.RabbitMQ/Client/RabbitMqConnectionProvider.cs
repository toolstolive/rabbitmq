﻿using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using RabbitMQ.Client;
using ToolsToLive.RabbitMQ.Interfaces.Client;
using ToolsToLive.RabbitMQ.Model;

namespace ToolsToLive.RabbitMQ.Client
{
    public class RabbitMqConnectionProvider : IRabbitMqConnectionProvider
    {
        private readonly IOptions<RabbitMqSettings> _settings;
        private readonly ILogger<RabbitMqConnectionProvider> _logger;
        private readonly IConnectionFactory _connectionFactory;

        private readonly SemaphoreSlim _semaphoreSlim = new SemaphoreSlim(1, 1);
        private IConnection _connection;
        private bool _disposed;

        public RabbitMqConnectionProvider(
            IConnectionFactory connectionFactory,
            IOptions<RabbitMqSettings> settings,
            ILogger<RabbitMqConnectionProvider> logger)
        {
            _connectionFactory = connectionFactory;
            _settings = settings;
            _logger = logger;
        }

        public async Task<IConnection> GetConnectionAsync(CancellationToken cancellationToken = default)
        {
            try
            {
                await _semaphoreSlim.WaitAsync(cancellationToken);

                if (_connection == null)
                {
                    _connection = await _connectionFactory.CreateConnectionAsync(_settings.Value.ConnectionName, cancellationToken);
                    _logger.LogDebug("Connection created");
                }
                else
                {
                    if (!_connection.IsOpen)
                    {
                        _connection = await _connectionFactory.CreateConnectionAsync(_settings.Value.ConnectionName, cancellationToken);
                        _logger.LogDebug("Connection restored");
                    }
                }

                return _connection;
            }
            finally
            {
                _semaphoreSlim.Release();
            }
        }

        /// <summary>
        /// This method should only be called when the application is closed.
        /// The connection must be long-lived.
        /// </summary>
        public async ValueTask DisposeAsync()
        {
            await DisposeAsync(true);
        }

        protected virtual async Task DisposeAsync(bool disposing)
        {
            if (_disposed)
            {
                return;
            }

            if (disposing)
            {
                _disposed = true;

                if (_connection != null)
                {
                    if (_connection.IsOpen)
                    {
                        await _connection.DisposeAsync();
                        _logger.LogDebug("Connection terminated (connection object destroyed)");
                    }
                    _connection = null;
                }
            }
        }
    }
}
