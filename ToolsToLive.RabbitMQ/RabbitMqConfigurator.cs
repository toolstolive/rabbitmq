using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using ToolsToLive.RabbitMQ.Interfaces;
using ToolsToLive.RabbitMQ.Interfaces.Client;
using ToolsToLive.RabbitMQ.Interfaces.ClientsPool;
using ToolsToLive.RabbitMQ.Interfaces.RoutingBuilders;
using ToolsToLive.RabbitMQ.Model;

namespace ToolsToLive.RabbitMQ
{
    public class RabbitMqConfigurator : IRabbitMqConfigurator
    {
        private readonly IExchangeNameBuilder _exchangeNameBuilder;
        private readonly IQueueNameBuilder _queueNameBuilder;
        private readonly IRabbitMqClientsPool _clientsPool;
        private readonly ILogger<RabbitMqConfigurator> _logger;

        public RabbitMqConfigurator(
            IExchangeNameBuilder exchangeNameBuilder,
            IQueueNameBuilder queueNameBuilder,
            IRabbitMqClientsPool clientsPool,
            ILogger<RabbitMqConfigurator> logger)
        {
            _exchangeNameBuilder = exchangeNameBuilder;
            _queueNameBuilder = queueNameBuilder;
            _clientsPool = clientsPool;
            _logger = logger;
        }

        public async Task ConfigureAsync<TRoute>(CancellationToken cancellationToken = default)
            where TRoute : Enum
        {
            try
            {
                using (var poolClient = _clientsPool.GetPoolClient())
                {
                    var rabbitMqClient = await poolClient.GetRabbitMqClientAsync(cancellationToken);
                    var exchangeName = _exchangeNameBuilder.Build();
                    var delayedExchangeName = _exchangeNameBuilder.BuildForDelayedQueues();

                    await rabbitMqClient.ExchangeDeclareAsync(exchangeName, cancellationToken);
                    await rabbitMqClient.ExchangeDeclareAsync(delayedExchangeName, cancellationToken);

                    await DefineDelayedQueueAsync(rabbitMqClient, exchangeName, delayedExchangeName, SendingDelay.Delay1, cancellationToken);
                    await DefineDelayedQueueAsync(rabbitMqClient, exchangeName, delayedExchangeName, SendingDelay.Delay2, cancellationToken);
                    await DefineDelayedQueueAsync(rabbitMqClient, exchangeName, delayedExchangeName, SendingDelay.Delay3, cancellationToken);
                    await DefineDelayedQueueAsync(rabbitMqClient, exchangeName, delayedExchangeName, SendingDelay.Delay4, cancellationToken);
                    await DefineDelayedQueueAsync(rabbitMqClient, exchangeName, delayedExchangeName, SendingDelay.Delay5, cancellationToken);
                    await DefineDelayedQueueAsync(rabbitMqClient, exchangeName, delayedExchangeName, SendingDelay.Delay6, cancellationToken);
                    await DefineDelayedQueueAsync(rabbitMqClient, exchangeName, delayedExchangeName, SendingDelay.Delay7, cancellationToken);

                    foreach (var routeObj in Enum.GetValues(typeof(TRoute)))
                    {
                        await DefineQueueAsync(rabbitMqClient, exchangeName, (TRoute)routeObj, cancellationToken);
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error while configuring RabbitMQ");
                throw;
            }
        }

        public async Task ClearAsync<TRoute>(CancellationToken cancellationToken = default)
            where TRoute : Enum
        {
            try
            {
                using (var poolClient = _clientsPool.GetPoolClient())
                {
                    var rabbitMqClient = await poolClient.GetRabbitMqClientAsync(cancellationToken);
                    var exchangeName = _exchangeNameBuilder.Build();
                    var delayedExchangeName = _exchangeNameBuilder.BuildForDelayedQueues();

                    await rabbitMqClient.DeleteExchangeAsync(exchangeName, cancellationToken);
                    await rabbitMqClient.DeleteExchangeAsync(delayedExchangeName, cancellationToken);

                    await rabbitMqClient.DeleteQueueAsync(_queueNameBuilder.Build(SendingDelay.Delay1), cancellationToken);
                    await rabbitMqClient.DeleteQueueAsync(_queueNameBuilder.Build(SendingDelay.Delay2), cancellationToken);
                    await rabbitMqClient.DeleteQueueAsync(_queueNameBuilder.Build(SendingDelay.Delay3), cancellationToken);
                    await rabbitMqClient.DeleteQueueAsync(_queueNameBuilder.Build(SendingDelay.Delay4), cancellationToken);
                    await rabbitMqClient.DeleteQueueAsync(_queueNameBuilder.Build(SendingDelay.Delay5), cancellationToken);
                    await rabbitMqClient.DeleteQueueAsync(_queueNameBuilder.Build(SendingDelay.Delay6), cancellationToken);
                    await rabbitMqClient.DeleteQueueAsync(_queueNameBuilder.Build(SendingDelay.Delay7), cancellationToken);

                    foreach (var routeObj in Enum.GetValues(typeof(TRoute)))
                    {
                        var queueName = _queueNameBuilder.Build((TRoute)routeObj);
                        await rabbitMqClient.DeleteQueueAsync(queueName, cancellationToken);
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error during RabbitMQ cleanup");
                throw;
            }
        }

        private async Task DefineQueueAsync<TRoute>(IRabbitMqClient client, string exchangeName, TRoute route, CancellationToken cancellationToken = default)
            where TRoute : Enum
        {
            var queueName = _queueNameBuilder.Build(route);
            await client.QueueDeclareAsync(queueName, null, cancellationToken);
            await client.QueueBindAsync(exchangeName, queueName, $"*.{route.ToString()}.*", cancellationToken);
        }

        private async Task DefineDelayedQueueAsync(IRabbitMqClient client, string exchangeName, string delayedExchangeName, SendingDelay delay, CancellationToken cancellationToken = default)
        {
            var queueName = _queueNameBuilder.Build(delay);

            await client.QueueDeclareAsync(
                queueName,
                new Dictionary<string, object>
                {
                    { "x-dead-letter-exchange", exchangeName },
                    { "x-queue-mode", "lazy" },
                },
                cancellationToken);

            await client.QueueBindAsync(delayedExchangeName, queueName, $"{delay}.*.*", cancellationToken);
        }
    }
}
