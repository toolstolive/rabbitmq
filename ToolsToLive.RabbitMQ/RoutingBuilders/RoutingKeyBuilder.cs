﻿using System;
using System.Text;
using ToolsToLive.RabbitMQ.Interfaces.RoutingBuilders;
using ToolsToLive.RabbitMQ.Model;

namespace ToolsToLive.RabbitMQ.RoutingBuilders
{
    public class RoutingKeyBuilder : IRoutingKeyBuilder
    {
        public string Build<T>(T targetRoute, string thirdKey = null) where T : Enum
        {
            return Build(targetRoute, SendingDelay.NoDelay, thirdKey);
        }

        public string Build(string targetRoute, string thirdKey = null)
        {
            return Build(targetRoute, SendingDelay.NoDelay, thirdKey);
        }

        public string Build<T>(T targetRoute, SendingDelay delay, string thirdKey = null) where T : Enum
        {
            return Build(targetRoute.ToString(), delay, thirdKey);
        }

        public string Build(string targetRoute, SendingDelay delay, string thirdKey = null)
        {
            var sb = new StringBuilder();

            sb.Append(delay.ToString());
            sb.Append('.');
            sb.Append(targetRoute);
            sb.Append('.');
            sb.Append(thirdKey ?? string.Empty);

            return sb.ToString();
        }
    }
}
