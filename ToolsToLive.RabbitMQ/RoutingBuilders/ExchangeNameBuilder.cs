﻿using System;
using System.Text;
using Microsoft.Extensions.Options;
using ToolsToLive.RabbitMQ.Interfaces.RoutingBuilders;
using ToolsToLive.RabbitMQ.Model;

namespace ToolsToLive.RabbitMQ.RoutingBuilders
{
    public class ExchangeNameBuilder : IExchangeNameBuilder
    {
        public string AppName { get; private set; }
        public string AppVersion { get; private set; }

        public ExchangeNameBuilder(IOptions<RabbitMqSettings> options)
        {
            AppName = options.Value.AppName;
            AppVersion = options.Value.AppVersion;
        }

        public string Build()
        {
            var sb = new StringBuilder();
            sb.Append(AppName.ToLowerInvariant());
            sb.Append('-');
            sb.Append(AppVersion.ToLowerInvariant());
            sb.Append("-exchange");

            return sb.ToString();
        }

        public string BuildForDelayedQueues()
        {
            var sb = new StringBuilder();
            sb.Append(AppName.ToLowerInvariant());
            sb.Append('-');
            sb.Append(AppVersion.ToLowerInvariant());
            sb.Append("-with-delay-exchange");

            return sb.ToString();
        }
    }
}
