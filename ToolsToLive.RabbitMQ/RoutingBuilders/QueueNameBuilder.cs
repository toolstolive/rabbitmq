﻿using System;
using System.Text;
using Microsoft.Extensions.Options;
using ToolsToLive.RabbitMQ.Interfaces.RoutingBuilders;
using ToolsToLive.RabbitMQ.Model;

namespace ToolsToLive.RabbitMQ.RoutingBuilders
{
    public class QueueNameBuilder : IQueueNameBuilder
    {
        public string AppName { get; private set; }
        public string AppVersion { get; private set; }

        public QueueNameBuilder(IOptions<RabbitMqSettings> options)
        {
            AppName = options.Value.AppName;
            AppVersion = options.Value.AppVersion;
        }

        /// <summary>
        /// Building a queue name for a specific route (values from the enum)
        /// </summary>
        public string Build<T>(T targetRoute) where T : Enum
        {
            var sb = new StringBuilder();
            sb.Append(AppName.ToLowerInvariant());
            sb.Append('-');
            sb.Append(AppVersion.ToLowerInvariant());
            sb.Append('-');

            var queueNameSuffix = StringSplitter(targetRoute.ToString());
            sb.Append(queueNameSuffix);
            sb.Append("-queue");

            return sb.ToString();
        }

        /// <summary>
        /// Building a queue name for a "delay queue" -- messages are sent to these queues for "temporary storage".
        /// Messages are transferred to the main queues according to the route after the expiration of the message lifetime.
        /// </summary>
        public string Build(SendingDelay delay)
        {
            var sb = new StringBuilder();
            sb.Append(AppName.ToLowerInvariant());
            sb.Append('-');
            sb.Append(AppVersion.ToLowerInvariant());

            sb.Append("-with-");
            sb.Append(delay.ToString().ToLowerInvariant());
            sb.Append("-queue");

            return sb.ToString();
        }

        /// <summary>
        /// Building a queue name for events
        /// </summary>
        public string Build(string eventType, string appKey)
        {
            var sb = new StringBuilder();
            sb.Append(AppName.ToLowerInvariant());
            sb.Append('-');
            sb.Append(AppVersion.ToLowerInvariant());
            sb.Append("-event-bus-");
            sb.Append(eventType.ToLowerInvariant());
            sb.Append('-');
            sb.Append(appKey.ToLowerInvariant());
            sb.Append("-queue");

            return sb.ToString();
        }

        private static string StringSplitter(string stringToSplit)
        {
            var wasPreviousUppercase = false;
            var sb = new StringBuilder();

            foreach (var c in stringToSplit)
            {
                if (char.IsUpper(c))
                {
                    if (wasPreviousUppercase)
                    {
                        sb.Append(char.ToLowerInvariant(c));
                    }
                    else
                    {
                        if (sb.Length > 0)
                        {
                            sb.Append('-');
                        }

                        sb.Append(char.ToLowerInvariant(c));
                    }
                    wasPreviousUppercase = true;
                }
                else
                {
                    wasPreviousUppercase = false;
                    sb.Append(c);
                }
            }

            return sb.ToString();
        }
    }
}
