using System.Diagnostics;

namespace ToolsToLive.RabbitMQ.Observability
{
    public static class Tracing
    {
        /// <summary>
        /// The name of Activity Source.
        /// To enable tracing you need to use .AddSource(ToolsToLive.RabbitMQ.Observability.Tracing.ActivitySourceName)
        /// </summary>
        public const string ActivitySourceName = "ToolsToLive.RabbitMQ";

        private static ActivitySource _activitySource = new ActivitySource(ActivitySourceName);

        /// <summary>
        /// Activity source with default name <see cref="ActivitySourceName"/>
        /// Can be overwritten, e.g. during DI setup
        /// </summary>
        public static ActivitySource ActivitySource
        {
            get
            {
                return _activitySource;
            }
            internal set
            {
                _activitySource = value;
            }
        }
    }
}
