﻿using System;

namespace ToolsToLive.RabbitMQ.Interfaces
{
    public interface IMessageSerializer
    {
        TMessage Deserialize<TMessage>(ReadOnlyMemory<byte> body) where TMessage : new();
        byte[] Serialize<TMessage>(TMessage message) where TMessage : new();
    }
}
