﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace ToolsToLive.RabbitMQ.Interfaces.Client
{
    public interface IRabbitMqClient : IAsyncDisposable
    {
        Task SendAsync(ReadOnlyMemory<byte> message, string exchangeName, string routingKey, TimeSpan? delay = null, CancellationToken cancellationToken = default);
        Task ListenAsync(string queueName, Func<ReadOnlyMemory<byte>, Task> onReceived, ushort prefetchCount, CancellationToken cancellationToken = default);
        Task SingleAckAsync(ulong deliveryTag, CancellationToken cancellationToken = default);
        Task StopListeningAsync(CancellationToken cancellationToken = default);
        Task QueueDeclareAsync(string queueName, Dictionary<string, object> arguments = null, CancellationToken cancellationToken = default);
        Task ExchangeDeclareAsync(string exchangeName, CancellationToken cancellationToken = default);
        Task QueueBindAsync(string exchangeName, string queueName, string routingKey, CancellationToken cancellationToken = default);
        Task DeleteQueueAsync(string queueName, CancellationToken cancellationToken = default);
        Task DeleteExchangeAsync(string exchangeName, CancellationToken cancellationToken = default);
    }
}
