﻿using System;
using System.Threading;
using System.Threading.Tasks;
using RabbitMQ.Client;

namespace ToolsToLive.RabbitMQ.Interfaces.Client
{
    public interface IRabbitMqConnectionProvider : IAsyncDisposable
    {
        Task<IConnection> GetConnectionAsync(CancellationToken cancellationToken = default);
    }
}
