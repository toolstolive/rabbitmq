﻿namespace ToolsToLive.RabbitMQ.Interfaces.Client
{
    public interface IRabbitMqClientFactory
    {
        IRabbitMqClient CreateClient();
    }
}
