﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace ToolsToLive.RabbitMQ.Interfaces
{
    public interface IMessageListenerFactory
    {
        Task<IMessageListener> CreateAndStartListeningAsync<TMessage>(string queueName, Func<TMessage, Task> onReceived, ushort prefetchCount, CancellationToken cancellationToken = default) where TMessage : new();

        Task<IMessageListener> CreateAndStartListeningAsync<TRoute, TMessage>(TRoute targetRoute, Func<TMessage, Task> onReceived, ushort prefetchCount, CancellationToken cancellationToken = default) where TMessage : new() where TRoute : Enum;
    }
}
