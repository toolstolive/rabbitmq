﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace ToolsToLive.RabbitMQ.Interfaces
{
    public interface IMessageListener : IAsyncDisposable
    {
        /// <summary>
        /// Subscribes to the specified queue, with specified the handler and additional settings
        /// </summary>
        /// <typeparam name="TMessage">Message type (for deserialization)</typeparam>
        /// <param name="queueName">Queue name</param>
        /// <param name="onReceived">Handler function</param>
        /// <param name="prefetchCount">Prefetch count - The listener takes all messages from the queue for processing, but no more than prefetchCount</param>
        /// <param name="cancellationToken">Cancellation token for this operation.</param>
        Task ListenAsync<TMessage>(string queueName, Func<TMessage, Task> onReceived, ushort prefetchCount, CancellationToken cancellationToken = default) where TMessage : new();
    }
}
