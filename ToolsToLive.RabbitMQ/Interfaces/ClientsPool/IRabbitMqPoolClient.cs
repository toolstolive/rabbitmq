﻿using System;
using System.Threading;
using System.Threading.Tasks;
using ToolsToLive.RabbitMQ.Interfaces.Client;

namespace ToolsToLive.RabbitMQ.Interfaces.ClientsPool
{
    public interface IRabbitMqPoolClient : IDisposable
    {
        Task<IRabbitMqClient> GetRabbitMqClientAsync(CancellationToken cancellationToken);
    }
}
