﻿using System;

namespace ToolsToLive.RabbitMQ.Interfaces.ClientsPool
{
    public interface IRabbitMqClientsPool : IAsyncDisposable
    {
        IRabbitMqPoolClient GetPoolClient();
    }
}
