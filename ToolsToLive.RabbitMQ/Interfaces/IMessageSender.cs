﻿using System;
using System.Threading.Tasks;
using System.Threading;
using ToolsToLive.RabbitMQ.Model;

namespace ToolsToLive.RabbitMQ.Interfaces
{
    public interface IMessageSender
    {
        Task SendAsync<TMessage>(TMessage message, string exchangeName, string route, TimeSpan? delay = null, CancellationToken cancellationToken = default)
            where TMessage : new();

        Task SendAsync<TMessage>(TMessage message, string exchangeName, string route, SendingDelay delay, CancellationToken cancellationToken = default)
            where TMessage : new();

        Task SendAsync<TRoute, TMessage>(TMessage message, TRoute targetRoute, CancellationToken cancellationToken = default)
            where TMessage : new() where TRoute : Enum;

        Task SendAsync<TRoute, TMessage>(TMessage message, TRoute targetRoute, SendingDelay delay, CancellationToken cancellationToken = default)
            where TMessage : new() where TRoute : Enum;
    }
}
