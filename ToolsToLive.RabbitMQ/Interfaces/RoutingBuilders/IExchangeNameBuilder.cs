﻿namespace ToolsToLive.RabbitMQ.Interfaces.RoutingBuilders
{
    public interface IExchangeNameBuilder
    {
        string Build();
        string BuildForDelayedQueues();
    }
}
