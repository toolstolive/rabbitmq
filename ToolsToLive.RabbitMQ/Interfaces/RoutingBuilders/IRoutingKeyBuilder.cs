﻿using System;
using ToolsToLive.RabbitMQ.Model;

namespace ToolsToLive.RabbitMQ.Interfaces.RoutingBuilders
{
    public interface IRoutingKeyBuilder
    {
        string Build<T>(T targetRoute, string thirdKey = null) where T : Enum;

        string Build(string targetRoute, string thirdKey = null);

        string Build<T>(T targetRoute, SendingDelay delay, string thirdKey = null) where T : Enum;

        string Build(string targetRoute, SendingDelay delay, string thirdKey = null);
    }
}
