﻿using System;
using ToolsToLive.RabbitMQ.Model;

namespace ToolsToLive.RabbitMQ.Interfaces.RoutingBuilders
{
    public interface IQueueNameBuilder
    {
        string Build<T>(T targetRoute) where T : Enum;
        string Build(SendingDelay delay);
        string Build(string eventType, string appKey);
    }
}
