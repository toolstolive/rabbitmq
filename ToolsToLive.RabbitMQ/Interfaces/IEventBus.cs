﻿using System;
using System.Threading;
using System.Threading.Tasks;
using ToolsToLive.RabbitMQ.Model;

namespace ToolsToLive.RabbitMQ.Interfaces
{
    public interface IEventBus
    {
        /// <summary>
        /// Subscribing to events.
        /// Each new subscription creates its own queue (if it was not created earlier) into which messages from the broker fall.
        /// Returns a subscription.
        /// </summary>
        /// <typeparam name="TMessage">Message type (type of class transmitted through the broker)</typeparam>
        /// <param name="eventType">Type (name) of the event.</param>
        /// <param name="appKey">The application key. Must be unique, for event broadcasting. Subscribing two apps with the same AppKey to the same event connect a listener to the same queue As a result, the event will receive only one of them.</param>
        /// <param name="onReceived">The function that will be called when the event is received.</param>
        /// <param name="prefetchCount">The number of messages that the listener will pull out of the queue at a time (and process them one at a time).</param>
        /// <param name="cancellationToken">Cancellation token for this operation.</param>
        /// <returns>Subscription on which to call Unsubscribe() (or Dispose()) method when disabling the application and/or stopping the listener.</returns>
        Task<Subscription> SubscribeAsync<TMessage>(string eventType, string appKey, Func<TMessage, Task> onReceived, ushort prefetchCount, CancellationToken cancellationToken = default)
            where TMessage : class, new();

        /// <summary>
        /// Refusal to receive further messages.
        /// Actually deletes the queue to which event messages are forwarded. All messages in the queue will be lost.
        /// There is NO need to unsubscribe if the service is temporarily stopped, for example, for an update.
        /// </summary>
        /// <param name="eventType">Type (name) of the event.</param>
        /// <param name="appKey">The application key that was specified when creating the subscriptionю</param>
        /// <param name="cancellationToken">Cancellation token for this operation.</param>
        Task DeleteQueueAsync(string eventType, string appKey, CancellationToken cancellationToken = default);

        /// <summary>
        /// Publishing (firing) an event.
        /// The message is sent to all bound queues (queues are created and bound when the Subscribe() method is called).
        /// If there is no subscription, the message is not sent anywhere.
        /// </summary>
        /// <typeparam name="TMessage">Message type (type of class transmitted through the broker)</typeparam>
        /// <param name="eventType">Type (name) of the event.</param>
        /// <param name="message">Message sent to the message broker.</param>
        /// <param name="cancellationToken">Cancellation token for this operation.</param>
        Task PublishAsync<TMessage>(string eventType, TMessage message, CancellationToken cancellationToken = default) where TMessage : class, new();
    }
}
