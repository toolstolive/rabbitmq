﻿using System;
using System.Threading.Tasks;
using System.Threading;

namespace ToolsToLive.RabbitMQ.Interfaces
{
    public interface IRabbitMqConfigurator
    {
        Task ConfigureAsync<TRoute>(CancellationToken cancellationToken = default)
            where TRoute : Enum;

        Task ClearAsync<TRoute>(CancellationToken cancellationToken = default)
            where TRoute : Enum;
    }
}
