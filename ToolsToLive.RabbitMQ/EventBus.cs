﻿using System.Threading.Tasks;
using System.Threading;
using System;
using ToolsToLive.RabbitMQ.Interfaces.ClientsPool;
using ToolsToLive.RabbitMQ.Interfaces.RoutingBuilders;
using ToolsToLive.RabbitMQ.Interfaces;
using ToolsToLive.RabbitMQ.Model;

namespace ToolsToLive.RabbitMQ
{
    public class EventBus : IEventBus
    {
        private readonly IExchangeNameBuilder _exchangeNameBuilder;
        private readonly IRoutingKeyBuilder _routingKeyBuilder;
        private readonly IQueueNameBuilder _queueNameBuilder;
        private readonly IMessageListenerFactory _listenerFactory;
        private readonly IMessageSender _sender;
        private readonly IRabbitMqClientsPool _clientsPool;

        public EventBus(
            IExchangeNameBuilder exchangeNameBuilder,
            IRoutingKeyBuilder routingKeyBuilder,
            IQueueNameBuilder queueNameBuilder,
            IRabbitMqClientsPool clientsPool,
            IMessageListenerFactory listenerFactory,
            IMessageSender sender
            )
        {
            _exchangeNameBuilder = exchangeNameBuilder;
            _routingKeyBuilder = routingKeyBuilder;
            _queueNameBuilder = queueNameBuilder;
            _clientsPool = clientsPool;
            _listenerFactory = listenerFactory;
            _sender = sender;
        }

        /// <inheritdoc />
        public async Task<Subscription> SubscribeAsync<TMessage>(string eventType, string appKey, Func<TMessage, Task> onReceived, ushort prefetchCount, CancellationToken cancellationToken = default)
            where TMessage : class, new()
        {
            var exchangeName = _exchangeNameBuilder.Build();
            var queueName = _queueNameBuilder.Build(eventType, appKey);
            var routingKey = _routingKeyBuilder.Build("EventBus", eventType);

            using (var clientPool = _clientsPool.GetPoolClient())
            {
                var rabbitMqClient = await clientPool.GetRabbitMqClientAsync(cancellationToken);
                await rabbitMqClient.QueueDeclareAsync(queueName, null, cancellationToken);
                await rabbitMqClient.QueueBindAsync(exchangeName, queueName, routingKey, cancellationToken);
            }

            var listener = await _listenerFactory.CreateAndStartListeningAsync(queueName, onReceived, prefetchCount, cancellationToken);
            var subscription = new Subscription(listener, queueName, eventType, appKey);

            return subscription;
        }

        /// <inheritdoc />
        public async Task DeleteQueueAsync(string eventType, string appKey, CancellationToken cancellationToken = default)
        {
            using (var clientPool = _clientsPool.GetPoolClient())
            {
                var rabbitMqClient = await clientPool.GetRabbitMqClientAsync(cancellationToken);
                var queueName = _queueNameBuilder.Build(eventType, appKey);
                await rabbitMqClient.DeleteQueueAsync(queueName, cancellationToken);
            }
        }

        /// <inheritdoc />
        public async Task PublishAsync<TMessage>(string eventType, TMessage message, CancellationToken cancellationToken = default) where TMessage : class, new()
        {
            await _sender.SendAsync(
                message: message,
                exchangeName: _exchangeNameBuilder.Build(),
                route: _routingKeyBuilder.Build("EventBus", eventType),
                cancellationToken: cancellationToken);
        }
    }
}
