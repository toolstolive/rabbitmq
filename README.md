# ToolsToLive.RabbitMQ

## How to start using it (configuring DI)
```
public void ConfigureServices(IServiceCollection services)
{
    var rabbitMqConfigSection = Configuration.GetSection("RabbitMqSettings");
    services.AddRabbitMqMessageHandler(rabbitMqConfigSection);
{}
```

## Configuration example (appsettings.json)

```json
    "RabbitMqSettings": {
        "Host": "localhost",
        "Port": "5672",
        "UserName": "mySuperUser",
        "Password": "mySuperPassword"
    },
```

Or:
```
    "RabbitMqSettings": {
        "ConnectionString": "amqp://guest:guest@localhost:5672/",
    },
```


## Configuring routes
For the initial configuration of routes (creation of exchangers, queues and their binding), you need to call the ConfigureRabbitMQ<TRoute>() method.
This method is an extension method for IServiceProvider.
The <TRoute> type parameter is an enum containing all the necessary routes

```
public enum Routes
{
    SendModel,
    SomethingElse
}

public void Configure(IApplicationBuilder app, IServiceProvider serviceProvider)
{
    serviceProvider.ConfigureRabbitMQ<Routes>();
}
```

## Sending a message to a queue
```
public class MyService
{
    private readonly IMessageSender _sender;

    public MyService(IMessageSender messageSender)
    {
        _sender = messageSender;
    }

    public void DoSometing()
    {
        //...

        var model = new Model
        {
            SomeField = "Some content",
        };

        _sender.Send(model, Routes.SendModel);
        //_sender.Send(model, Routes.SendModel, SendingDelay.Delay4); // For sending with a delay of 5 minutes

        //...
    }
}
```

## Receiving and processing messages

```
public class MyHostedService : IHostedService
{
    private readonly IServiceProvider _serviceProvider;
    private readonly IMessageListenerFactory _listenerFactory;

    private IMessageListener _listener;

    public MyHostedService(IServiceProvider serviceProvider,
                            IMessageListenerFactory listenerFactory)
    {
        _serviceProvider = serviceProvider;
        _listenerFactory = listenerFactory;
    }

    public async Task StartAsync(CancellationToken cancellationToken)
    {
        _serviceProvider.ConfigureRabbitMQ<Routes>();
        _listener = _listenerFactory.CreateAndStartListening<Routes, Model>(Routes.SendModel, HandleRabbitMessage, 5);
    }

    public async Task StopAsync(CancellationToken cancellationToken)
    {
        _listener.Dispose();
        Serilog.Log.CloseAndFlush();
    }

    private async Task HandleRabbitMessage(Model msg)
    {
        await DoIt();
    }
}
```


## EventBus
EventBus works independently of routes, but uses the same Exchange.
When subscribing to a certain event, a queue is created to which messages from the exchanger will be sent.
Messages from the exchanger are sent to all bound queues.

```
public class MyService
{
    private readonly IEventBus _eventBus;

    public MyService(IEventBus eventBus)
    {
        _eventBus = eventBus;
    }

    public void DoSometing()
    {
        // Both subscriptions will receive a message because different appkeys are specified
        var subscription1 = _eventBus.Subscribe<SomethingCreatedEvent>("some_event", "app1", Handler1, 1);
        var subscription2 = _eventBus.Subscribe<SomethingCreatedEvent>("some_event", "app2", Handler2, 1);

        var e = new SomethingCreatedEvent
        {
            Id = 1243,
            Content = "Some content",
        };
        _eventBus.Publish("some_event", e); ;


        subscription1.Dispose(); // or Unsubscribe();
        subscription2.Dispose();


        // Calling the DeleteQueue() method would delete the queues. This is usually not required, as the service shutdown temporary
        // and after restarting, it is usually necessary to process all received messages.

        // _eventBus.DeleteQueue(""some_event"", "app1");
        // _eventBus.DeleteQueue(""some_event"", "app2");
    }

    private async Task Handler1(SomethingCreatedEvent e)
    {
        await DoIt1();
    }

    private async Task Handler2(SomethingCreatedEvent e)
    {
        await DoIt2();
    }
}
```
